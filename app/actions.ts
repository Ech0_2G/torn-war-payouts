'use server'

export async function fetch_torn_war_data(_prevState: any, formData: FormData) {
	const rawFormData = {
		apiKey: formData.get('apiKey') as string,
		warID: formData.get('warID') as string,
		factionID: formData.get('factionID') as string,
		payoutAmount: formData.get('payoutAmount') as string,
		payoutPercent: formData.get('payoutPercent') as string
	}

	const staticData = await fetch(`https://api.torn.com/torn/${rawFormData.warID}?selections=rankedwarreport&key=${rawFormData.apiKey}`)

	return await staticData.json().then(async data => {
		const faction = data.rankedwarreport.factions[rawFormData.factionID];
		const payoutAmount = Number(rawFormData.payoutAmount)
		const payoutPercent = Number(rawFormData.payoutPercent)

		const pay_per_hit = ((payoutAmount * (payoutPercent * 0.01)) / faction.attacks)
		const USDollar = new Intl.NumberFormat('en-US', {
			style: 'currency',
			currency: 'USD',
			minimumFractionDigits: 0,
			maximumFractionDigits: 0
		})

		const memberData = Object.values(faction.members).map((member: any) => {
			let payout = Math.ceil(member.attacks * pay_per_hit)

			return {
				name: member.name,
				attacks: member.attacks,
				payout: payout,
				pretty_payout: USDollar.format(payout)
			}
		})

		const pawnShopResponse = await fetch(`https://api.torn.com/torn/?selections=pawnshop&key=${rawFormData.apiKey}`)
		const pointsMarketValue = await pawnShopResponse.json().then(data => {
			return data.pawnshop.points_value * faction.rewards.points
		})


		let itemMarketValues = 0

		for await (const item_id of Object.keys(faction.rewards.items)) {
			const item = await fetch(`https://api.torn.com/torn/${item_id}?selections=items&key=${rawFormData.apiKey}`)
			const itemValue = await item.json().then(item_data => {
				return item_data.items[item_id].market_value
			})

			itemMarketValues += itemValue * faction.rewards.items[item_id].quantity
		}

		const marketValuePayout = pointsMarketValue + itemMarketValues

		return {
			apiKey: rawFormData.apiKey,
			warID: rawFormData.warID,
			factionID: rawFormData.factionID,
			payoutAmount: rawFormData.payoutAmount,
			payoutPercent: rawFormData.payoutPercent,
			marketValuePayout: marketValuePayout,
			name: faction.name,
			attacks: faction.attacks,
			per_hit_payment: pay_per_hit,
			members: memberData
		}
	})
}

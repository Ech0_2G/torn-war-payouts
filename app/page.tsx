'use client'

import { useActionState } from 'react'
import { fetch_torn_war_data } from '@/app/actions'

export default function Home() {

	const initialState = {
		apiKey: '',
		warID: '',
		factionID: '',
		payoutAmount: '',
		payoutPercent: '',
		marketValuePayout: 0,
		name: '',
		attacks: 0,
		per_hit_payment: 0,
		members: [
			{
				name: '',
				attacks: 0,
				payout: 0.00,
				pretty_payout: "$0"
			}
		]
	}

	const [state, formAction] = useActionState(fetch_torn_war_data, initialState)

	const members = state.members.sort((a, b) => b.attacks - a.attacks).map(member => {
		return (
			<tr key={member.name}>
				<td className="border border-rosePine-foam px-6 py-3">{member.name}</td>
				<td className="border border-rosePine-foam px-6 py-3">{member.attacks}</td>
				<td
					className="border border-rosePine-foam px-6 py-3"
					title={`${state.per_hit_payment} x ${member.attacks} = ${member.pretty_payout}`}
				>{member.pretty_payout} <span className="text-sm mx-3">({member.payout})</span></td>
			</tr>
		)
	})

	const USDollar = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
		minimumFractionDigits: 0,
		maximumFractionDigits: 0
	})

	return (
		<main className="flex min-h-screen flex-col items-center justify-between px-24 py-8 bg-rosePine-surface text-rosePine-text">
			<form className="text-rosePine-text" action={formAction}>
				<div className="flex flex-wrap -mx-3 mb-6">
					<div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
						<label htmlFor="apiKey" className="block text-md font-bold leading-6 text-rosePine-text px-4">API Key</label>
						<div className="mt-2">
							<input type="text" name="apiKey" id="apiKey" className="block flex-1 border border-rosePine-foam bg-rosePine-surface py-2 px-4 rounded-full" />
						</div>
					</div>
					<div className="w-full md:w-1/2 px-3">
						<label htmlFor="warID" className="block text-md font-bold leading-6 text-rosePine-text px-4">War ID</label>
						<div className="mt-2">
							<input type="text" name="warID" id="warID" className="block flex-1 border border-rosePine-foam bg-rosePine-surface py-2 px-4 rounded-full" />
						</div>
					</div>
				</div>

				<div className="flex flex-wrap -mx-3 mb-6">
					<div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
						<label htmlFor="factionID" className="block text-md font-bold leading-6 text-rosePine-text px-4">Faction ID</label>
						<div className="mt-2">
							<input type="text" name="factionID" id="factionID" className="block flex-1 border border-rosePine-foam bg-rosePine-surface py-2 px-4 rounded-full" />
						</div>
					</div>
					<div className="w-full md:w-1/2 px-3">
						<label htmlFor="payoutAmount" className="block text-md font-bold leading-6 text-rosePine-text px-4">Payout</label>
						<div className="mt-2">
							<input type="number" name="payoutAmount" id="payoutAmount" min="0" step="0.01" className="block flex-1 border border-rosePine-foam bg-rosePine-surface py-2 px-4 rounded-full" />
						</div>
					</div>
				</div>

				<div className="flex flex-wrap -mx-3 mb-6">
					<div className="w-full md:w-1/2 px-3 mb-6 md:mb-0 align-middle">
						<label htmlFor="payoutPercent" className="block text-md font-bold leading-6 text-rosePine-text px-4">Payout Percent</label>
						<div className="mt-2">
							<input type="number" name="payoutPercent" id="payoutPercent" min="0" step="1" max="100" className="block flex-1 border border-rosePine-foam bg-rosePine-surface py-2 px-4 rounded-full" />
						</div>
					</div>
					<div className="w-full md:w-1/2 px-3 content-end">
						<div className="mt-2">
							<button type="submit" className="rounded-lg bg-rosePine-pine px-4 py-3 text-md font-bold text-rosePine-text shadow-sm hover:bg-rosePine-rose">Submit</button>
						</div>
					</div>
				</div>

			</form>

			<header className="mt-8 text-rosePine-text">
				<h1 className="text-xl font-bold">{state?.name}</h1>
				<p title={`${state?.marketValuePayout}`}>Points + Items Market Value: {USDollar.format(Number(state?.marketValuePayout))}</p>
				<p title={state?.payoutAmount}>Manual Payout: {USDollar.format(Number(state?.payoutAmount))}</p>
				<p title={`${state?.per_hit_payment}`}>Hit Price: {USDollar.format(state?.per_hit_payment)}</p>
			</header>
			<table className="mt-2 table-auto border-collapse border border-rosePine-foam text-rosePine-text">
				<thead>
					<tr>
						<th className="border border-rosePine-foam text-rosePine-text px-6 py-3">Member</th>
						<th className="border border-rosePine-foam text-rosePine-text px-6 py-3">Hits</th>
						<th className="border border-rosePine-foam text-rosePine-text px-6 py-3">Payout</th>
					</tr>
				</thead>
				<tbody>
					{members}
				</tbody>
			</table>
		</main >
	);
}
